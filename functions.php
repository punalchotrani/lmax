<?php
/* Enqueuing Styles*/
function enqueue_lmax_styles()
{
    wp_enqueue_style('bundle', get_template_directory_uri() . '/dist/css/styles.min.css');
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i,900,900i|Source+Sans+Pro:400,700,900', false );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/bower_components/font-awesome/css/font-awesome.min.css', false );
}

add_action('wp_enqueue_scripts', 'enqueue_lmax_styles');

/* Enqueuing Scripts*/
function enqueue_lmax_scripts() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri().'/dist/js/jquery.min.js', false, '3.1.1', true);
    wp_register_script( 'bootstrap-js', get_template_directory_uri().'/dist/js/bootstrap.js', array('jquery'), '3.3.6',true);
    wp_register_script( 'script', get_template_directory_uri() . '/dist/js/scripts.min.js', array('jquery'), '1.0', true);
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bootstrap-js' );
    wp_enqueue_script( 'script' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_lmax_scripts' );

