<?php
/*
 Template Name: BackEnd Test
*/
get_header();
?>
    <body>
<main class="container">
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-7"><h1>Special reports</h1></div>
    </div>
    <div class="row">
        <div class="col-md-5 sidebar-content">
            <div class="col-md-6 no-padding">
                <img src="https://dummyimage.com/560x315/109/fff&text=image1" alt="image-1"
                     class="img-responsive expand-img">
                <div id="myModal" class="img-modal">
                    <span class="close">&times;</span>
                    <img class="modal-content center" id="img01">
                    <div class="caption text-center"></div>
                </div>
            </div>
            <div class="col-md-6 no-padding">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item"
                            src="https://www.youtube.com/embed/qOPClGrwnNs?rel=0&amp;controls=0&amp;showinfo=0"
                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.</p>
            <ul>
                <?php
                $args = array(
                    'post_type' => 'post',
                    'category_name' => 'special-reports',
                    'posts_per_page' => 3,
                    'orderby' => 'date'
                );
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $the_query->the_post(); ?>
                        <li>
                            <button data-toggle="#<?php echo sanitize_title(get_the_title()); ?>">
                                <span class="glyphicon glyphicon-chevron-right"
                                      aria-hidden="true"></span> <?php the_title(); ?>
                            </button>
                        </li>
                        <?php
                    }
                }
                wp_reset_postdata(); ?>
            </ul>
            <div class="well">
                <?php
                $args = array(
                    'post_type' => 'post',
                    'category_name' => 'special-reports',
                    'posts_per_page' => 3,
                    'orderby' => 'date'
                );
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) {
                    while ($the_query->have_posts()) {
                        $the_query->the_post(); ?>
                        <div class="well-content" id="<?php echo sanitize_title(get_the_title()); ?>">
                            <?php echo get_the_content(); ?>
                        </div>
                        <?php
                    }
                }
                wp_reset_postdata(); ?>
            </div>
            <h2>Sed quia consequuntur magni dolores</h2>
            <div class="col-md-4 col-md-push-8 no-padding-col">
                <div class="slideshow full-width">
                    <div>
                        <img src="https://dummyimage.com/200x150/e635e6/fff"
                             class="img-responsive imgLeftSpacing full-width">
                    </div>
                    <div>
                        <img src="https://dummyimage.com/200x150/2473bd/ffffff"
                             class="img-responsive imgLeftSpacing full-width">
                    </div>
                    <div>
                        <img src="https://dummyimage.com/200x150/56bd26/ffffff"
                             class="img-responsive imgLeftSpacing full-width">
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-md-pull-4 no-padding-col">
                <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                    totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae
                    dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit,
                    sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam
                    est.</p>

            </div>

        </div>
    </div>
    <div class="row">
        <div class="container">
            <div class="col-md-12"></div>
        </div>
    </div>
</main>

<?php get_footer(); ?>