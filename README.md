# Imax Candidate test

I decided to create a standalone WP theme, and apply the tasks into the theme which can be worked on in a version control environment, and would be easier to share.

Download the theme and activate it in a wordpress install.

### Get Started

- Open up git shell and enter the following commands to make any front end changes

- npm install

- bower install

- grunt watch


## Common Questions

### Where is the Css & Js compiled?
It is compiled in the dist or distribution folder. This folder will contain anything processed or compiled by Grunt

### When you upload the site or page to the server which are the files you have to upload?
All you need is the compiled files in the dist folder. The readme file helps a future developer find the source code in a public/private repo

- dist
- README.md
- humans.txt
- index.php
- robots.txt

## How do you get grunt to stop watching the file?
All you have to do is close the shell window. If you enter the command "start grunt watch" it will open two shell windows. One watching and the other for new commands. I usually just do "grunt watch" and leave it watching all day as I go in and out of the day. Then when I go home I commit my changes to github and close the command shell.