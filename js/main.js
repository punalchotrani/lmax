$(function() {
  $("button[data-toggle]").on("click", function(e) {
    e.preventDefault();
    let selector = $(this).data("toggle");
    $(".well-content").hide();
    $(selector).show();
  });

  /* Model script */
  let model = document.querySelector('.img-modal'),
      modelImg = document.querySelector('#img01'),
      img = document.querySelector('.expand-img'),
      captionText = document.querySelector('.caption'),
      close = document.querySelector('.close');

  img.addEventListener('click', function() {
    model.style.display = "block";
    modelImg.src = this.src;
    captionText.innerHTML = this.alt;
  });

  close.addEventListener('click', () => {
    model.style.display = "none";
  });

  model.addEventListener('click', () => {
    model.style.display = "none";
  });

  /*slideshow*/
  $(".slideshow > div:gt(0)").hide();

  setInterval(function() {
    $('.slideshow > div:first')
      .fadeOut(1000)
      .next()
      .fadeIn(1000)
      .end()
      .appendTo('.slideshow');
  },  2000);
});